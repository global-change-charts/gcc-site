import {BasePageProps, Chart} from "../lib/types";
import Layout from "./Layout";
import i18next from "i18next";
import Link from "next/link";
import {BSection} from "./bulma-helpers";

interface SectionProps extends BasePageProps {
    charts: Chart[],
    title: () => string,
    slug: string
}

export function Section({charts, language, title, slug}: SectionProps) {
    return <Layout>
        <section className="container" style={{padding: 16}}>
            <nav className="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li>
                        <Link href={`/${language}`}>
                            <a>{i18next.t("home")}</a>
                        </Link>
                    </li>
                    <li className="is-active">
                        <Link href={`${language}/${slug}`}>
                            <a href="#" aria-current="page">{title()}</a>
                        </Link>
                    </li>
                </ul>
            </nav>
        </section>

        <BSection>
            <h1 className="is-1 has-text-centered is-uppercase is-light">
                {title()}
            </h1>

            <hr
                style={{
                    width: "600px",
                    maxWidth: "50%",
                    margin: "auto",
                    backgroundColor: "darkgrey",
                }}
            />
        </BSection>
        <BSection>
            <div className="columns is-multiline">
                {charts.map((chart: Chart) => (
                    <div key={chart.slug} className="column is-one-third">
                        <Link href={`/${language}/${slug}/${chart.slug}`}>
                            <a>
                                <picture>
                                    <source srcSet={chart.image.thumbnailWebp} type="image/webp"/>
                                    <source srcSet={chart.image.thumbnailJpeg} type="image/jpeg"/>
                                    <img src={chart.image.thumbnailJpeg} alt="Alt Text!"/>
                                </picture>
                            </a>
                        </Link>
                    </div>
                ))}
            </div>
        </BSection>
    </Layout>
}
