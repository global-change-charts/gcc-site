import {BasePageProps} from "../lib/types";
import Layout from "./Layout";
import Link from "next/link";
import i18next from "i18next";

interface ChartProps extends BasePageProps {
    sectionSlug: string
    chartSlug: string
    chartTitle: () => string
    sectionTitle: () => string
    children: React.ReactNode
}

export function ChartLayout({language, sectionSlug, chartSlug, chartTitle, sectionTitle, children}: ChartProps) {
    return <Layout>
        <section className="container" style={{padding: 16}}>
            <nav className="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li>
                        <Link href={`/${language}`}>
                            <a>{i18next.t("home")}</a>
                        </Link>
                    </li>
                    <li>
                        <Link href={`/${language}/${sectionSlug}`}>
                            <a>{sectionTitle()}</a>
                        </Link>
                    </li>
                    <li className="is-active">
                        <Link href={`/${language}/${sectionSlug}/${chartSlug}`}>
                            <a href="#" aria-current="page">{chartTitle()}</a>
                        </Link>
                    </li>
                </ul>
            </nav>
        </section>
        {children}
    </Layout>
}
