import Link from "next/link";
import {useRouter} from "next/router";
import i18next from "i18next";
import {changeLanguageURL} from "../lib/lang";
import {useState} from "react";

function ChangeLocale() {
    const router = useRouter();

    return <div className="navbar-item has-dropdown is-hoverable">
        <a className="navbar-link">
            {i18next.t("language")}
        </a>

        <div className="navbar-dropdown">
            {i18next.languages?.map((l) => (
                <Link key={l} href={changeLanguageURL(router.asPath, l)} prefetch={false}>
                    <a className="navbar-item">
                        {l}
                    </a>
                </Link>
            ))}
        </div>
    </div>
}

const Header = () => {
    const [active, setActive] = useState(false);

    return (
        <header>
            <nav className="navbar is-dark" role="navigation" aria-label="main navigation">
                <div className="container">
                    <div className="navbar-brand">
                        <Link href={`/${i18next.language}/`}>
                            <a className="navbar-item is-size-3 is-size-4-mobile">
                                Global Change Charts
                            </a>
                        </Link>

                        <a
                            role="button"
                            className="navbar-burger burger"
                            aria-label="menu"
                            aria-expanded="false"
                            data-target="navbarContent"
                            onClick={() => setActive(!active)}
                        >
                            <span aria-hidden="true"/>
                            <span aria-hidden="true"/>
                            <span aria-hidden="true"/>
                        </a>
                    </div>

                    <div className={"navbar-menu " + (active ? "is-active" : "")} id="navbarContent">
                        <div className="navbar-end">
                            <Link href={`/${i18next.language}/`}>
                                <a className="navbar-item">
                                    {i18next.t("menu")}
                                </a>
                            </Link>
                            <Link href={`/${i18next.language}/about`}>
                                <a className="navbar-item">
                                    {i18next.t("about")}
                                </a>
                            </Link>
                            <ChangeLocale />
                        </div>
                    </div>
                </div>
            </nav>
        </header>
    );
};

export default Header;
