import React from "react";


export function BSection({children, className}: {children: React.ReactNode, className?: string}) {
    return <section className={"section content container " + className}>
        {children}
    </section>
}

