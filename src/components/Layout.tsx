import Head from 'next/head';

import i18next from 'i18next';
import Header from "./Header";

export interface LayoutProps {
    children: React.ReactNode
}

export function Footer() {
    return (
        <footer className="footer has-text-centered">
            <div className="content has-text-grey">
                Copyright Global Change Charts 2021
            </div>
        </footer>
    );
}

const Layout = function ({children}: LayoutProps) {
    return (
        <div style={{display: "flex", minHeight: "100vh", flexDirection: "column"}}>
            <Head>
                <title>{i18next.t('siteMeta.title')}</title>
                <meta charSet="utf-8"/>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
                <link rel="preload" href="/fonts/lato-v17-latin-regular.woff2" as="font" type="font/woff2"/>
            </Head>
            <div style={{flex: 1}}>
                <Header />
                <main>
                    {children}
                </main>
            </div>
            <Footer />
        </div>
    );
};
export default Layout;
