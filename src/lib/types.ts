
export interface BasePageProps {
    language: string
}

interface ChartImage {
    path: string
    thumbnailWebp: string
    thumbnailJpeg: string
}

export interface Chart {
    slug: string
    image: ChartImage
    title: () => string
}
