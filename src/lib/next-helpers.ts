import {getAllLanguageSlugs, getLanguage} from "./lang";

export async function getStaticPaths() {
    const paths = getAllLanguageSlugs();
    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({params}) {
    const language = getLanguage(params.lang);
    return {
        props: {
            language,
        },
    };
}
