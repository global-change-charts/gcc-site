import {defaultLanguage, languages} from '../i18n/config';

export function getAllLanguageSlugs() {
	return languages.map((lang) => {
		return { params: { lang: lang } };
	});
}

export function getLanguage(lang) {
	return languages.includes(lang) ? lang : defaultLanguage;
}

export function changeLanguageURL(url: string, lng: string) {
	const splitted = url.split("/")
	return ["", lng, ...splitted.slice(2)].join("/")
}
