import React from "react"
import '../i18n/init';

import i18next from 'i18next';
import {AppProps} from "next/app";
import "bulma/css/bulma.min.css"
import "./global.css"

function App({ Component, pageProps }: AppProps) {
	i18next.changeLanguage(pageProps.language);
	return <Component {...pageProps} />
}

export default App;
