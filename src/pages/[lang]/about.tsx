import React from "react";
import i18next from "i18next";
import Layout from "../../components/Layout";
import {getAllLanguageSlugs, getLanguage} from "../../lib/lang";

function About () {
    return (
        <Layout>
            <section className="section container content">
                <h1 className="is-title">
                    {i18next.t('about')}
                </h1>
                <p>
                    To write ...
                </p>
            </section>

        </Layout>
    );
}

export default About;

export async function getStaticPaths() {
    const paths = getAllLanguageSlugs();
    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const language = getLanguage(params.lang);
    return {
        props: {
            language,
        },
    };
}
