import {BasePageProps} from "../../../lib/types";
import {BSection} from "../../../components/bulma-helpers";
import i18next from "i18next";
import {ChartLayout} from "../../../components/ChartLayout";


export default function SeaTurtles(props: BasePageProps) {
    return <ChartLayout {...props}
                        chartSlug="sea-turtles"
                        chartTitle={() => i18next.t("charts.biodiversity.sea-turtles")}
                        sectionSlug="biodiversity"
                        sectionTitle={() => i18next.t("sections.biodiversity")}
    >
        <BSection>
            <h1 className="is-title is-1 has-text-centered">
                {i18next.t("charts.biodiversity.sea-turtles")}
            </h1>

            <img
                src="/charts/info_seaturtles.svg"
                alt="ChartLayout about sea turtles"
            />
        </BSection>
    </ChartLayout>
}

export {getStaticPaths, getStaticProps} from "../../../lib/next-helpers";
