import i18next from "i18next";
import {BasePageProps, Chart} from "../../../lib/types";
import {Section} from "../../../components/Section";

const CHARTS: Chart[] = [
    {
        slug: "sea-turtles",
        title: () => i18next.t("charts.biodiversity.sea-turtles"),
        image: {
            path: "/charts/info_seaturtles.svg",
            thumbnailWebp: "/charts/info_seaturtles.webp",
            thumbnailJpeg: "/charts/info_seaturtles.jpeg",
        }
    },
    {
        slug: "sea-turtles",
        title: () => i18next.t("charts.biodiversity.sea-turtles"),
        image: {
            path: "/charts/info_seaturtles.svg",
            thumbnailWebp: "/charts/info_seaturtles.webp",
            thumbnailJpeg: "/charts/info_seaturtles.jpeg",
        }
    },
    {
        slug: "sea-turtles",
        title: () => i18next.t("charts.biodiversity.sea-turtles"),
        image: {
            path: "/charts/info_seaturtles.svg",
            thumbnailWebp: "/charts/info_seaturtles.webp",
            thumbnailJpeg: "/charts/info_seaturtles.jpeg",
        }
    },
    {
        slug: "sea-turtles",
        title: () => i18next.t("charts.biodiversity.sea-turtles"),
        image: {
            path: "/charts/info_seaturtles.svg",
            thumbnailWebp: "/charts/info_seaturtles.webp",
            thumbnailJpeg: "/charts/info_seaturtles.jpeg",
        }
    },
]

export default function SectionPage(props: BasePageProps) {
    return <Section charts={CHARTS}
                    slug={"biodiversity"}
                    title={() => i18next.t("sections.biodiversity")}
                    {...props} />
}

export {getStaticPaths, getStaticProps} from "../../../lib/next-helpers";
