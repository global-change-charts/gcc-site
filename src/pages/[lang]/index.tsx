import i18next from 'i18next';
import Link from "next/link";

import Layout from '../../components/Layout';
import {BasePageProps} from "../../lib/types";

interface Section {
    title: () => string,
    slug: string,
    is_empty: boolean,
    backgroundColor?: string,
}

const SECTIONS: Section[] = [
    {
        slug: 'climate',
        is_empty: false,
        title: () => i18next.t('sections.climate'),
        backgroundColor: "#50a8c9",
    },
    {
        slug: 'biodiversity',
        is_empty: false,
        title: () => i18next.t('sections.biodiversity'),
        backgroundColor: "#8fbe6d",
    },
    {
        slug: 'industry',
        is_empty: false,
        title: () => i18next.t('sections.industry'),
        backgroundColor: "#c3254b",
    },
    {
        slug: 'agriculture',
        is_empty: true,
        title: () => i18next.t('sections.agriculture')
    },
    {
        slug: 'waste',
        is_empty: true,
        title: () => i18next.t('sections.waste')
    },
    {
        slug: 'energy',
        is_empty: true,
        title: () => i18next.t('sections.energy')
    },
    {
        slug: 'transport',
        is_empty: true,
        title: () => i18next.t('sections.transport')
    },
    {
        slug: 'digital-technology',
        is_empty: true,
        title: () => i18next.t('sections.digital-technology')
    },
    {
        slug: 'building',
        is_empty: true,
        title: () => i18next.t('sections.building')
    },
    {
        slug: 'politics',
        is_empty: true,
        title: () => i18next.t('sections.politics')
    },
    {
        slug: 'research',
        is_empty: true,
        title: () => i18next.t('sections.research')
    },
    {
        slug: 'diseases',
        is_empty: true,
        title: () => i18next.t('sections.diseases')
    },
    {
        slug: 'demographic-growth',
        is_empty: true,
        title: () => i18next.t('sections.demographic-growth')
    },
    {
        slug: 'psychology-and-behavior',
        is_empty: true,
        title: () => i18next.t('sections.psychology-and-behavior')
    },
    {
        slug: 'ecofeminism',
        is_empty: true,
        title: () => i18next.t('sections.ecofeminism')
    },
]

function Home({language}: BasePageProps) {
    return <Layout>
        <section className="section">
            <div className="container content has-text-centered">
                <h1 className="title is-spaced is-1 has-text-weight-normal">
                    {i18next.t('home title')}
                </h1>

                <h2 className="subtitle is-3 has-text-weight-normal">
                    {i18next.t("home subtitle")}
                </h2>

                <div className="columns is-multiline" style={{marginTop: 48}}>
                    {SECTIONS.map(({title, slug, backgroundColor, is_empty}) => (
                        <div className="column is-one-third" key={slug}>
                            <div
                                className="box is-uppercase is-title is-size-5 has-text-weight-medium"
                                style={(!is_empty && backgroundColor) ? {backgroundColor} : {backgroundColor: "#afafaf"}}
                            >
                                {is_empty ?
                                    <div className="has-text-white">
                                        {title()}
                                    </div>
                                    :
                                    <Link href={`/${language}/${slug}`}>
                                        <a className="has-text-white">
                                            {title()}
                                        </a>
                                    </Link>
                                }
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    </Layout>;
}

export default Home;

export {getStaticProps, getStaticPaths} from "../../lib/next-helpers";
