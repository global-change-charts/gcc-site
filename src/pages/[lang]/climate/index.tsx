import i18next from "i18next";
import {BasePageProps, Chart} from "../../../lib/types";
import {Section} from "../../../components/Section";

const CHARTS: Chart[] = [
    {
        slug: "test",
        title: () => i18next.t("charts.climate.test"),
        image: {
            path: "test/image.png",
            thumbnailJpeg: "test/image.jpeg",
            thumbnailWebp: "test/image.webp",
        }
    },
]

export default function SectionPage(props: BasePageProps) {
    return <Section charts={CHARTS}
                    slug={"climate"}
                    title={() => i18next.t("sections.climate")}
                    {...props} />
}

export {getStaticPaths, getStaticProps} from "../../../lib/next-helpers";
