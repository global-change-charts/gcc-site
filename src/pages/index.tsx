import { useEffect } from 'react';
import { useRouter } from 'next/router';

import i18next from 'i18next';

import {defaultLanguage, languages} from "../i18n/config";

export default function Home({ allLangsData }) {
	const router = useRouter();

	useEffect(() => {
		const { pathname } = router;
		if (pathname == '/') {
			const lng = i18next.language.substring(0, 2);
			if (languages.find((elt => elt === lng), lng))
				router.push('/' + lng);
			else
				router.push('/' + defaultLanguage)
		}
	});

	return null;
}
