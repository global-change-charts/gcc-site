const sharp = require("sharp");
const meow = require("meow");

const IMAGE_WIDTH = 400;

function resizer(file?: string) {
    if (!file) {
        console.error("Cannot parse argument 1");
        return 1;
    }

    const fileWithoutExt = file.substr(0, file.lastIndexOf("."));
    const image = sharp(file)
        .resize(IMAGE_WIDTH)

    image.toFile(fileWithoutExt + ".webp").then((value => {
        console.log(`Written image (${value.height}x${value.width}) to format ${value.format} to ${fileWithoutExt + ".webp"}`)
    }))
    image.toFile(fileWithoutExt + ".jpg").then((value => {
        console.log(`Written image (${value.height}x${value.width}) to format ${value.format} to ${fileWithoutExt + ".jpg"}`)
    }))
}

const cli = meow(`
    Usage:
        $ resizer path/to/file.svg
`);

resizer(cli.input[0]);

export {}
