# Global Change Charts -- website

## Installation

1. Install [NodeJS](https://nodejs.org/)
2. Install [`yarn`](https://yarnpkg.com/getting-started/install) (usually `npm i -g yarn` is enough)
3. Install dependancies with
    ```shell
    yarn install
    ```
   
## Run

To run a dev session:
```shell
yarn dev
```

To export to HTML/CSS files, run:
```shell
yarn export
```

## Structure

The project uses [nextjs](https://nextjs.org/) to generate a static site.
We uses [i18next](https://www.i18next.com/) to provide an internationalization framework.
